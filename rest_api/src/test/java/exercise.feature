Feature: Pet Store
  Background:
    Given url baseUrl
    * def params = {"id":0,"category":{"id":0,"name":"string"},"name":"adidog","photoUrls":["string"],"tags":[{"id":0,"name":"string"}],"status":"available"}

  Scenario: Get all pets available
    #Available
    Given path '/pet/findByStatus'
    When method GET
    And param status = 'available'
    Then status 200
    #And match response == read('expected-availables.json')

  Scenario: Create, update and Delete the same pet
    #New
    Given path '/pet'
    And request params
    When method POST
    Then status 200
    And match response == read('expected-new.json')
    * set params.id = response.id

    #Update
    Given path '/pet'
    And set params.status = 'sold'
    And request params
    When method PUT
    Then status 200
    And match response == params

    #Delete
    Given path '/pet/' + params.id
    When method DELETE
    Then status 200
