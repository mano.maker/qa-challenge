## Dependencies

Java JRE 11 and above

## Execute

`./gradlew test`

## Report

The report is created on `/build/karate-reports/karate-summary.html`

## Code Structure

```
rest_api
  |
  +-- build.gradle  > Configuration file for project
  +-- settings.gradle  > Configuration file for properties
  \-- src
        |
        +-- test/java
                |
                +-- karate-config.js    > Configuration file for Karate DSL
                +-- logback-test.xml    > Configuration file for Logging
                +-- KarateTests.java    > Initializer class for Karate DSL
                +-- expected-new.json   > File with data to be used in tests or validations 
                \-- exercise.feature    > Test scenario for the challenge
```

## Implementation

Karate DSL already has internal implementation for step and validation, so there no need to write Java code besides the startup class
The steps are all in one file, because I couldn't have time to split and link the Scenarios 
(ex. the update required to create one pet before, and to delete to clean the database)

## Assertions

> Get all pets available

The assertion should be if the status code is 200 and if we control the database check if the response is deep equal to the previous data.

> Create a pet

The assertion should be if the status code is 200, if response has the Id with number and the parameters are the same that the request

> Update the pet

The assertion should be if the status code is 200 and if response parameters are the same that the request

> Delete the pet

The assertion should be if the status code is 200 and search for the petId to verify if don't exist
