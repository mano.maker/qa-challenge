## Dependencies

Node.js 12 or 14 and above, [System requirements](https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements)

## Install

`yarn`

## Execute

`yarn cy:run`

## Report

Besides the logs on the terminal, a quick report is created on `/cypress/reports/html/index.html`

## Code Structure

```
web_fe
  |
  +-- cypress.json  > Configuration file for cypress
  +-- package.json  > Configuration file for project
  +-- tsconfig.json > Configuration file for Typescript
  \-- cypress
        |
        +-- fixtures   > Folder to keep json files with data to be used in tests or validations
        +-- integration
        |       |
        |       +-- common           > Folder with common steps and parameters types to be included in all feature files
        |       +-- shopping         > Folder with steps definition to be included in shopping.feature
        |       \-- shopping.feature > Test scenario for the challenge
        |
        +-- plugins    > Configuration for the cypress plugins used in the project
        +-- reports    > Generated report from the tests execution
        +-- sceenshots > Folder with pictures of the failing tests
        +-- support
        |      |
        |      +-- commands.ts > File with custom commands to be used during the tests
        |      +-- index.d.ts  > File with interface for custom commands
        |      \-- index.ts    > File with configuration to initialize Cypress runner
        \-- videos     > Folder with videos of the tests execution
```

## Implementation

The validation of a Given or When step should be included in the step so the feature file be more clean and the requirement validation is done at the end.
This means on every step must exist at least one validation (should or expect), also to take advantage of Cypress internal retries for the validation.

## Assertions

> Customer navigation through product categories: Phones, Laptops and Monitors

Because the page uses ajax and don't redirect to a page, the assertion should be if all products are from the categorie requested and if all are shown in the screen (divided by pages)

> Navigate to "Laptop" → "Sony vaio i5" and click on "Add to cart". Accept pop up confirmation.
> Navigate to "Laptop" → "Dell i7 8gb" and click on "Add to cart". Accept pop up confirmation.

_Note: During the implementation the pop up never appear_
The assertion should be if the product is visible on the list of the category and if the button to add to cart is visible and adds the value of the product to total.
(if the pop up was shown another assertion was check if appear on _Add to Cart_)

> Navigate to "Cart" → Delete "Dell i7 8gb" from cart.

The assertion should be if the product is visible on the cart, after the click the product is removed from the cart and subtract the value of the product to total.

> Click on "Place order".

The assertion should be if the modal is visible

> Fill in all web form fields.
> Click on "Purchase"

The assertion should be if the alert is visible and the data is the same as entered in the form.

> Capture and log purchase Id and Amount.
> Assert purchase amount equals expected.

The assertion should be if the purchase amount is the equal to the total, saved when added to cart.

> Click on "Ok"

The assertion should be if the modal isn't visible
