Cypress.Commands.add('addToTotal', (value) => {
  cy.get('@total').then((totalStr) => {
    const total: number = +totalStr;
    const amount: number = total + value;
    cy.wrap(amount).as('total');
  });
});
