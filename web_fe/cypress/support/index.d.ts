declare namespace Cypress {
  interface Chainable {
    addToTotal(value: number): void;
  }
}
