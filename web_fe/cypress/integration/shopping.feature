Feature: adidas Web FE Automation

  Scenario: Buy from Demo Onine Shop
    Given I open store page
    Then I navigate to "Phones" category
    Then I navigate to "Laptops" category
    Then I navigate to "Monitors" category
    # When I add to cart the following product from category:
    #   | product      | category |
    #   | Sony vaio i5 | Laptops  |
    #   | Dell i7 8gb  | Laptops  |
    When I add to cart "Sony vaio i5" from Laptops page
    And I add to cart "Dell i7 8gb" from Laptops page
    And I navigate to "Cart" page
    And I delete "Dell i7 8gb" from Cart
    And I place an order with name "Cypress Testing", country "Portugal", city "Leiria", credit card "123456789", month "May", year "2021"
    Then Purchase amount should be equal to the sum of products
