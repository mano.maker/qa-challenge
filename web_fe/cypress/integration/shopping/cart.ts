import { When, Then } from 'cypress-cucumber-preprocessor/steps';

When('I delete {string} from Cart', (productName) => {
  cy.contains(productName)
    .should('be.visible')
    .siblings()
    .eq(1)
    .invoke('text')
    .then((priceStr) => {
      const price: number = -priceStr;
      cy.addToTotal(price);
    });
  cy.contains(productName).parent().find('a').click();
  cy.contains(productName).should('not.exist');
});

When(
  'I place an order with name {string}, country {string}, city {string}, credit card {string}, month {string}, year {string}',
  (name, country, city, cardNumber, cardMonth, cardYear) => {
    cy.get('.btn-success').click();
    cy.get('#totalm').should('be.visible');
    cy.get('#name').type(name);
    cy.get('#country').type(country);
    cy.get('#city').type(city);
    cy.get('#card').type(cardNumber);
    cy.get('#month').type(cardMonth);
    cy.get('#year').type(cardYear);
    cy.get('#orderModal .btn-primary').click();
    cy.get('.sweet-alert h2').should(
      'have.text',
      'Thank you for your purchase!'
    );
  }
);

Then('Purchase amount should be equal to the sum of products', () => {
  cy.get('.sweet-alert p')
    .should('be.visible')
    .invoke('text')
    .then((purchaseInfo) => {
      const startId = purchaseInfo.indexOf(':') + 2;
      const endId = purchaseInfo.indexOf('Amount: ');

      const startAmount = purchaseInfo.indexOf(':', endId) + 2;
      const endAmount = purchaseInfo.indexOf('Card Number: ');

      const id = purchaseInfo.substring(startId, endId);
      const amount = purchaseInfo.substring(startAmount, endAmount);
      cy.log(`Purchase Id: ${id}`);
      cy.log(`Purchase Amount: ${amount}`);
      cy.get('@total').then((total) => {
        expect(amount).to.contain(total);
      });
    });
  cy.get('.sweet-alert .btn-primary').click();
  cy.get('.sweet-alert').should('not.be.visible');
});
