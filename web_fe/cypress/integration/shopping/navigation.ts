import { When } from 'cypress-cucumber-preprocessor/steps';

When('I navigate to {string} category', (category) => {
  cy.intercept('/entries').as('products');
  cy.intercept('POST', '/bycat').as('responseProducts');
  cy.get('.list-group').contains(category).click();
  //TODO check if all products are from the category
  // cy.wait('@response')
  //   .its('response.body.Items')
  //   .then((array) => {
  //     cy.get('#tbodyid > div').its('length').should('eq', array.length);
  //     array.forEach((product: string) => {
  //       const categoryCode =
  //         category === 'Laptops'
  //           ? 'notebook'
  //           : category.slice(0, -1).toLowerCase();
  //       expect(product).to.include({ cat: categoryCode });
  //     });
  //   });
  //cy.wait('@products');
  cy.wait('@responseProducts');
});

When('I navigate to {string} page', (page: string) => {
  cy.contains(page).click();
});
