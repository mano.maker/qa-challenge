import { defineParameterType, When } from 'cypress-cucumber-preprocessor/steps';

defineParameterType({
  name: 'categories',
  regexp: /(Phones|Laptops|Monitors)/,
  transformer: (s) => s,
  useForSnippets: false,
  preferForRegexpMatch: true,
});

When(
  'I add to cart {string} from {categories} page',
  (product: string, category: string) => {
    cy.intercept('/entries').as('products');
    cy.intercept('POST', '/bycat').as('responseProducts');
    cy.intercept('POST', '/addtocart').as('responseAddCart');
    cy.visit('/');
    cy.get('.list-group').contains(category).click();
    cy.wait('@products');
    cy.wait('@responseProducts');
    //TODO while product not on the page, click next
    cy.contains(product).should('be.visible').click();
    cy.get('h2').should('have.text', product);
    cy.get('.price-container')
      .invoke('text')
      .then((text) => {
        cy.get('@total').then((totalText) => {
          cy.addToTotal(+text.substring(1, text.indexOf(' ')));
        });
      });
    cy.get('.btn-success').click();
    cy.wait('@responseAddCart');
  }
);

When('I add to cart the following product from category:', (datatable) => {
  cy.log(datatable);
});
