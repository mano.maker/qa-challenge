import { Given } from 'cypress-cucumber-preprocessor/steps';

Given('I open store page', () => {
  cy.visit('/');
  cy.wrap(0).as('total');
  cy.get('#contcont').should('be.visible');
});
