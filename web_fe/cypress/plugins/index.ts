const browserify = require('@cypress/browserify-preprocessor');
const cucumber = require('cypress-cucumber-preprocessor').default;
const resolve = require('resolve');

/**
 * @type {Cypress.PluginConfig}
 */
declare let module: any;

module.exports = (on, config) => {
  require('cypress-mochawesome-reporter/plugin')(on);

  const options = {
    ...browserify.defaultOptions,
    typescript: resolve.sync('typescript', { baseDir: config.projectRoot }),
  };

  on('file:preprocessor', cucumber(options));
};
